. ${DATALOCAL_SARGAS}/sargassum-contouring/sargasse_virtualenv/bin/activate
DATE=`date --date='1 days ago' '+%Y-%m-%d'`
${DATALOCAL_SARGAS}/sargassum-contouring/src/sargassum-contouring.py ${DATALOCAL_SARGAS}/sargassum-contouring/config/configuration_LAAS.yaml $DATE
deactivate

