#!/usr/bin/env python
# coding: utf-8

# # Sargasses
# Contourage pour délimiter les zones de présence de sargasses
import logging
import logging.handlers as handlers
import copy
import numpy as np
import netCDF4 as netcdf
from scipy import interpolate
from skimage import measure
import yaml
import geopandas as gpd
from fiona.crs import from_epsg
from shapely.geometry import Polygon
import sys
import motuclient
import os
import re
from lxml import etree
import shutil
import time
from dask.array.wrap import empty
from matplotlib import path
from click.termui import launch

from scipy.spatial import ConvexHull
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA

import traceback

t_start = time.time()
print_freq = 10

# generate "temporary" shapefiles to show the steps
generate_steps = False

def observationFile(parameters,p,sensorName):

	#retrieving the bounding box(minx, miny, maxx, maxy) tuple.

	box=p.bounds
	observationArea=''
	if box[0]<0:
		observationArea= observationArea + "W{:05d}".format(int(abs(box[0])*100))
	else:
		observationArea= observationArea + "E{:05d}".format(int(abs(box[0])*100))  
	if box[1]<0:
		observationArea= observationArea + "S{:04d}".format(int(abs(box[1])*100))
	else:
		observationArea= observationArea + "N{:04d}".format(int(abs(box[1])*100)) 
	if box[2]<0:
		observationArea= observationArea + "W{:05d}".format(int(abs(box[2])*100))
	else:
		observationArea= observationArea + "E{:05d}".format(int(abs(box[2])*100))    
	if box[3]<0:
		observationArea= observationArea + "S{:04d}".format(int(abs(box[3])*100))
	else:
		observationArea= observationArea + "N{:04d}".format(int(abs(box[3])*100))
		 

	observationDate=str(parameters['dataset_year'])+str(parameters['dataset_month'])+str(parameters['dataset_day'])+'120000'

	#Outputfilename creation
	outputFilename='SAR'+'-'+observationDate+'-'+observationDate+'-'+observationArea+'-'+str(sensorName)+'.xml'
	
	#my_logger.info('Creation observation file:  %s', parameters['mobidrift_output_directory']+outputFilename) 
			
	observation = etree.Element("Observation")
	observation.set("xmlns", "http://drift.cls.fr/XML/RecalInternalObservation")
	#Type of detection
	sourceType = etree.SubElement(observation, "SourceType")
	sourceType.text="saroutline"
	
	zone = etree.SubElement(observation, "PolygonZone")   
	zone.text= str(box[0])+' '+str(box[1])+' '+str(box[0])+' '+str(box[3])+' '+str(box[2])+' '+str(box[3])+' '+ str(box[2])+' '+ str(box[2])+' '+str(box[0])+' '+str(box[1])
	 
	observationItem = etree.SubElement(observation, "ObservationItem")
	observationItem.set("date", str(parameters['dataset_year'])+'-'+"{:02d}".format(int(parameters['dataset_month']))+'-'+"{:02d}".format(int(parameters['dataset_day']))+'T12:00:00.000Z')
	
	points=list(p.exterior.coords)
	for point in points:
		if (point is not points[ -1 ] ):
			feature = etree.SubElement(observationItem, "Feature")
			feature.set('latitude',str(point[1]))
			feature.set('longitude',str(point[0]))

	my_tree = etree.ElementTree(observation)
	with open(str(parameters['netcdf_output_directory'])+outputFilename, 'wb') as f:
		f.write(etree.tostring(my_tree))
		
	#my_logger.info('Move observation from %s into the final directory:  %s', str(parameters['netcdf_output_directory'])+outputFilename,parameters['mobidrift_output_directory']+outputFilename)   
	shutil.move(str(parameters['netcdf_output_directory'])+outputFilename, parameters['mobidrift_output_directory']+outputFilename)

# downloads datasets
def downloading(parameters,dataset,output_directory,output_filenename):
	try:
			(options, args) = motuclient.load_options()
			options.user = parameters['user_name']
			options.pwd = parameters['user_password']

			options.motu = 'http://motu-datastore.cls.fr/motu-web/Motu'
			options.service_id = "Sargassum-TDS" 
			options.product_id = dataset
			options.longitude_min = parameters['min_lon']
			options.longitude_max = parameters['max_lon']
			options.latitude_min = parameters['min_lat']
			options.latitude_max = parameters['max_lat']
			options.date_min = str(parameters['dataset_year'])+'-'+str(parameters['dataset_month'])+'-'+str(parameters['dataset_day'])
			options.date_max = str(parameters['dataset_year'])+'-'+str(parameters['dataset_month'])+'-'+str(parameters['dataset_day'])
			options.variable = str("raw_nfai;nfai").split(";")
			options.out_dir = output_directory
			options.out_name = output_filenename
			
			my_logger.info('Dowloading dataset %s ', options.product_id)
			my_logger.info('           date %s ', options.date_min)
			my_logger.info('           variable %s ', options.variable)
			print('Dowloading dataset ', options.product_id)
			print('           date ', options.date_min)
			print('           variable ', options.variable)
			 
			motuclient.motu_api.execute_request(options)   
			
	except Exception as e:
		my_logger.error('issue with motu download') 
		sys.exit(1)

# basic contouring using marching square
def contouring(parameters,output_directory,output_filenename):

	try:
		# Dataset is created contained diff type of data (NFAI, raw_NFAI, longitude, latitude, etc)
		f = netcdf.Dataset(output_directory+output_filenename)
		# g is NFAI data[0] (actual NFAI value I think)
		variable = f[parameters['variable_name']][0, :]

		# X-axis is longitude
		x = f[parameters['x_name']][:]
		# Y-axis is latitude
		y = f[parameters['y_name']][:]
		x_shift = 0 if max(x) <= 180.0 else -360.0
		x = x + x_shift

		# Creates interpolations from grid indices to lat lon (x) / lat(y)
		xinterp = interpolate.interp1d(np.arange(len(x), dtype='float32'), x)
		yinterp = interpolate.interp1d(np.arange(len(y), dtype='float32'), y)
		level=float(parameters['level_min'])
		
		#Remove NaN data
		variable.data[variable.data==32767]=-1000

		print("Start of contouring, mode %s on file %s" %(parameters['mode'],output_filenename))
		
		#Look for contour
		polygonArray = []
		
		idx = np.array([[(i,j) for j in range(variable.data.shape[1])] for i in range(variable.data.shape[0])]).reshape(np.prod(variable.data.shape),2)
		contours = measure.find_contours(variable.data, level)
		my_logger.info('Contouring task completed, %d contours found', len(contours))
	
		# m is the number of points of the longest
		# path in file
		m,c = 0,0
	
		my_logger.info('file already existing: %s', output_directory+output_filenename) 
		# build and register the paths
		for n, contour in enumerate(contours):
			if (len(contour[:, 1]) > parameters['contour_threlhold']):
			# closed unclosed contours (may not be accurate)
				if ((contour[0][0] != contour[-1][0]) or (contour[0][1] != contour[-1][1])):
					contour=np.append(contour, [contour[0]],axis=0)

				closed_path = path.Path(contour)

				# Get the points that lie within the closed path
				mask = closed_path.contains_points(idx,radius=1e-9).reshape(variable.shape)
				if len(variable.data[mask])>0:
					p = list(zip(xinterp(contour[:, 1]), yinterp(contour[:, 0])))
					m = max(m, len(p))
					if (len(p) > parameters['contour_threlhold']) and (variable.data[mask].max()>float(parameters['level_max'])):
						polygonArray.append(Polygon(p))
		my_logger.info('Editing of smaller contours, there are  %d contours left', len(polygonArray)) 
		
		# the array is return inside a single element list since we only have 1 class in this mode
		return [polygonArray]
		
	except Exception as e:
		my_logger.error('issue with contouring task')
		traceback.print_exc()
		print(e)
		sys.exit(1)

# 
def remove_artifact(parameters,output_directory,output_filenename):
	# Dataset is created contained diff type of data (NFAI, raw_NFAI, longitude, latitude, etc)
	f = netcdf.Dataset(output_directory+output_filenename)
	
	nfai = f['nfai'][0, :]
	raw_nfai = f['raw_nfai'][0, :]

	#Remove NaN data
	nfai.data[nfai.data==32767]=-1000
	raw_nfai.data[raw_nfai.data==32767]=-1000

	# 2D array (countours are separated in classes, which represent the "confidence" value)
	clusterClasses = []
	for i in range (parameters['class_num']):
		clusterClasses.append([])

	# We create 2 classes (1 for clusters with NFAI, 2 for clusters with only raw_NFAI)		
	print("[%.2f] [PRERUN] Transforming data" % (time.time() - t_start) )
	my_logger.info("[PRERUN] Transforming data")
	scale = parameters['scale']
	offset_x = parameters['min_lon']
	offset_y = parameters['min_lat']
	
	data = [[j*scale + offset_y , i*scale + offset_x] for j in range(len(nfai.data)) for i in range(len(nfai.data[0])) if (nfai.data[j,i]>parameters['level_min'])]
	raw_data = [[j*scale + offset_y , i*scale + offset_x] for j in range(len(raw_nfai.data)) for i in range(len(raw_nfai.data[0])) if (raw_nfai.data[j,i]>parameters['level_raw'])]

	data = np.asarray(data)
	raw_data = np.asarray(raw_data)

	# DBSCAN is computed on raw NFAI data first
	labels, core_samples_mask, n_clusters = compute_dbscan(parameters['dbscan_artifact_eps'], parameters['dbscan_artifact_min_samples'], raw_data)
	unique_labels = set(labels)
	
	print("[%.2f] [PRERUN] Total number of clusters %d" % ( (time.time() - t_start), n_clusters))
	my_logger.info("[PRERUN] Total number of clusters %d", n_clusters)

	artifact_clusters = []

	# for each cluster
	for k in unique_labels:
		# Outliers cluster
		if k == -1:
			continue
		# Create label mask
		if (k % int(n_clusters / print_freq) == 0):
			print("[%.2f] [PRERUN] Calculating mask #%d/%d" %( (time.time() - t_start), k, n_clusters))

		class_member_mask = (labels == k)
		full_points = raw_data[class_member_mask]

		# debug/verbose
		if generate_steps:
			clusterClasses[6].append(full_points)

		# at this point we are only interested in "supposed artifacts"
		if classification(parameters, full_points, nfai) == 3:
			# save the cluster as a list of points, if it's a supposed artifact
			artifact_clusters.append(full_points)

			# debug/verbose
			if generate_steps:
				clusterClasses[7].append(full_points)

	if (parameters['remove_artifact_mode'] == "soft"):
		# Artifacts are stored as a list of boolean (True = confirmed, False = discarded)
		boolArtifacts = scan_artifacts(artifact_clusters, raw_nfai, scale, parameters)
	elif (parameters['remove_artifact_mode'] == "hard"):
		boolArtifacts = [True for i in range(len(artifact_clusters))]
	else:
		print("ERROR: remove_artifact_mode can only be set to \"soft\" or \"hard\" ")
		sys.exit(1)
	
	for i in range(len(boolArtifacts)):
		# we found an artifact
		if boolArtifacts[i]:

			# debug/verbose
			if (generate_steps):
				# artifact is added as a class 4
				clusterClasses[4].append(artifact_clusters[i])

			# points describing the artifact
			for [j,i] in artifact_clusters[i]:
				# the artifact data is replaced by a cloud
				raw_nfai.data[ int((j-offset_y)/scale), int((i-offset_x)/scale) ] = -1000

	# ### DBSCAN on updated data
	raw_data = [[j*scale + offset_y , i*scale + offset_x] for j in range(len(raw_nfai.data)) for i in range(len(raw_nfai.data[0])) if (raw_nfai.data[j,i]>parameters['level_raw'])]
	raw_data = np.asarray(raw_data)
	
	# DBSCAN is computed on raw NFAI data first
	labels, core_samples_mask, n_clusters = compute_dbscan(parameters['dbscan_eps'], parameters['dbscan_min_samples'], raw_data)
	unique_labels = set(labels)
	
	print("[%.2f] Total number of clusters %d" %( (time.time() - t_start), n_clusters) )
	my_logger.info("Total number of clusters %d", n_clusters)
	
	# for each cluster
	for k in unique_labels:
		# Outliers cluster
		if k == -1:
			continue

		# print "progression"
		if (k %int(n_clusters / print_freq) == 0):
			print("[%.2f] Calculating mask #%d/%d" %((time.time() - t_start),k,n_clusters))

		class_member_mask = (labels == k)
		full_points = raw_data[class_member_mask]
		
		# debug/verbose
		if generate_steps:
			clusterClasses[8].append(full_points)
		
		# the cluster contains a "real" NFAI point
		clusterClass = classification(parameters, full_points, nfai)
		# the cluster is stored in the correct class number
		clusterClasses[clusterClass].append(full_points)

	# print number of cluster in each class
	for i, clusterClass in enumerate(clusterClasses):
		print("[%.2f] %d in class %d" % ((time.time() - t_start), len(clusterClass), i) )

	return clusterClasses

# regroup clusters
def cluster_cluster(parameters, clusterClasses):
	scale = parameters['scale']
	min_cluster_distance = 10*scale
	min_cluster_orientation = 0.2*np.pi
	
	newClusters = []
	
	# creates a PCA solver for 2D array (only need to be done once)
	pca = PCA(n_components=2, copy=True)
	
	# only interested in class0 and class1 cluster
	goodClusters = [ cluster for clusterClass in clusterClasses[:2] for cluster in clusterClass ]
	mediumClusters = [ cluster for clusterClass in clusterClasses[:3] for cluster in clusterClass ]
	
	# choose which set we are using
	workClusters = goodClusters
	
	n = len(workClusters)
	
	print("[%.2f] computing PCAs" % (time.time() - t_start) )
	# computes the PCA of each cluster
	clusterPCA_dir = []
	clusterPCA_ratio = []
	for cluster in workClusters:
		pca.fit(cluster)
		# PCA is elongated
		clusterPCA_dir.append(np.arctan(pca.components_[0][0] / pca.components_[0][1]))
		clusterPCA_ratio.append(pca.explained_variance_ratio_[0])

	print("[%.2f] computing distances" % (time.time() - t_start) )
	# matrix of (distance, pointA, pointB) between each cluster
	distanceMatrix = np.full( (n,n,3), (-1, None, None) )
	# computes each pair of distance in a matrix
	for i in range(n):
		for j in range (n):
			# same cluster
			if i==j:
				distanceMatrix[i,j] = 0
			else:
				distanceMatrix[i,j] = cluster_distance(workClusters[i], workClusters[j])

	print("[%.2f] finding close clusters" % (time.time() - t_start) )
	
	addToClusterList = []
	# for each tuple in the matrix
	for i in range(n):
		addToCluster = [i]
		
		for j in range(n):
			if i>j or i==j:
				continue
			(distance, minA, minB) = distanceMatrix[i,j]
			# clusters are close
			if distance < min_cluster_distance:
				# one of the clusters is elongated enough
				if max(clusterPCA_ratio[i], clusterPCA_ratio[j]) > 0.8:
					if clusterPCA_ratio[i] > clusterPCA_ratio[j]:
						primary = i
					else:
						primary = j

					centerA = np.mean(workClusters[i], axis=0)
					centerB = np.mean(workClusters[j], axis=0)

					# clusters alignement angle
					if (centerA[1]-centerB[1]) == 0:
						m = np.pi/2
					else:
						m = np.arctan((centerA[0]-centerB[0])/(centerA[1]-centerB[1]))

					# TEMPORARY #
					# if (centerA[0] > 15.51 and centerA[0] < 15.59 and centerA[1] < -60.4 and centerA[1] > -60.48):
					#	print("CenterA (%.4f, %.4f), centerB (%.4f, %.4f), varianceA %.2f, varianceB %.2f, m direction %.2f, dirA %.2f, dirB %.2f" % (centerA[1], centerA[0], centerB[1], centerB[0], clusterPCA_ratio[i], clusterPCA_ratio[j], m, clusterPCA_dir[i], clusterPCA_dir[j] ) )

					# the main cluster is oriented along the axis formed by the 2 clusters
					if abs(m - clusterPCA_dir[primary]) < min_cluster_orientation:
						# we note that j has to be added to i
						addToCluster.append(j)
						
		# updating previous clusters
		new = True
		for id in addToCluster:
			for clusterIDs in addToClusterList:
				if id in clusterIDs:
					# we make sure not to put the ID twice
					new = False
					for k in addToCluster:
						if id == k:
							continue
						clusterIDs.append(k)

		# when every cluster that needs to be added to i has been found
		if new:
			addToClusterList.append(addToCluster)

	for clusterIDs in addToClusterList:
		newCluster = workClusters[clusterIDs[0]]
		for id in clusterIDs[1:]:
			#print(id)
			newCluster = np.concatenate((newCluster, workClusters[id]))
		newClusters.append(newCluster)

	print("[%.2f] %d clusters appended together" % ( (time.time() - t_start), len(workClusters) - len(newClusters) ) )
	clusterClasses[5] = newClusters

# *estimates* the distance between two clusters
# returns the distance and the point from which it is measured on each cluster
def cluster_distance(clusterA, clusterB):
	# barycenter of each cluster
	centerA = np.mean(clusterA, axis=0)
	centerB = np.mean(clusterB, axis=0)

	# init minima
	minA = centerA
	minB = centerB
	minDistance = distance(minA, minB)

	# finds the min distance between clusterA and centerB
	for point in clusterA:
		dist = distance(point, centerB)
		if dist < minDistance:
			minA = point
			minDistance = dist
	
	# finds the min distance between clusterB and minA
	for point in clusterB:
		dist = distance(point, minA)
		if dist < minDistance:
			minB = point
			minDistance = dist
	
	return minDistance, minA, minB

# calculates the euclidian distance between two points
def distance(pointA, pointB):
	return np.sqrt( (pointA[0]-pointB[0])**2 + (pointA[1]-pointB[1])**2 )

# ranks clusters
def classification(parameters, cluster, nfai):
	offset_x = parameters['min_lon']
	offset_y = parameters['min_lat']
	scale = parameters['scale']

	shared_ammount = 0

	for [y,x] in cluster:
		new_x = int( (x-offset_x) / scale)
		new_y = int( (y-offset_y) / scale)
		if (nfai.data[new_y,new_x] > parameters['level_min']):
			shared_ammount += 1

	# class 0
	if (shared_ammount*100 / len(cluster) >= parameters['class_nfai_percent']):
		return 0
	# class 1
	elif (shared_ammount != 0):
		return 1
	# class 2
	elif (len(cluster) >= parameters['class_min_points']):
		return 2
	# class 3
	else:
		return 3

# ##############
# Compute DBSCAN
def compute_dbscan(e, m, X_):
	print("Running DBSCAN with eps=%.4f, min_samples=%d" % (e,m))
	my_logger.info("Running DBSCAN with eps=%.4f, min_samples=%d", e, m)
	
	# DBSCAN calculation
	db = DBSCAN(eps=e, min_samples=m).fit(X_)
	core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
	core_samples_mask[db.core_sample_indices_] = True
	labels = db.labels_

	# Number of clusters in labels, ignoring noise if present.
	n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
	n_noise_ = list(labels).count(-1)

	print('Estimated number of clusters: %d' % n_clusters_)
	print('Estimated number of noise points: %d' % n_noise_)
	
	return labels, core_samples_mask, n_clusters_

def shapefile(parameters,observationArea,polygonClasses,sensorName):
	# one shapefile for each class is created
	for k in range(len(polygonClasses)):
		polygonArray = polygonClasses[k]
		print("[%.2f] Writing shapefile %d/%d, %d shapes" %((time.time() - t_start), (k+1), len(polygonClasses), len(polygonArray) ) )
		# CRS management
		outdata = gpd.GeoDataFrame()
		outdata['geometry'] = None
		
		# Set the GeoDataFrame's coordinate system to WGS84
		outdata.crs = from_epsg(4326)
		# Save polygon in Shapefile
		i = 0
		if len(polygonArray)>0:
			for p in polygonArray:
				i = i + 1
				outdata.loc[i, outdata.geometry.name] = p
				# xml creation
				#observationFile(parameters,p,sensorName)
			
			shapefile_name= parameters['shapefile_directory']+'/' + \
								observationArea + "_" + parameters['mode'] + "_" + sensorName + '_' + \
								str(parameters['dataset_year'])+str(parameters['dataset_month'])+str(parameters['dataset_day'])+'_CLASS'+str(k)+'.shp'
			outdata.to_file(shapefile_name)
			my_logger.info('Creation of shapefile with %d contours: %s',len(polygonArray), shapefile_name)

def scan_artifacts(clusterArray, raw_nfai, scale, parameters):
	offset_x = parameters['min_lon']
	offset_y = parameters['min_lat']

	angles = []
	oob_counter = 0
	
	# for every potential artifact
	for cluster in clusterArray:
		# calculates the barycenter of the cluster
		[y,x] = np.mean(cluster, axis=0)
		#print([x,y])

		# largest dimension
		radius = 10*scale
		
		# for each point in the *squared* box
		min_x = int( (x-radius-offset_x) / scale)
		min_y = int( (y-radius-offset_y) / scale)
		max_x = int( (x+radius-offset_x) / scale)
		max_y = int( (y+radius-offset_y) / scale)

		# counts the number of time the "circle" goes outside the data range
		if (min_x < 0 or min_y < 0 or max_x > len(raw_nfai.data[0]) or max_y > len(raw_nfai.data) ):
			oob_counter += 1
		else:
			cloud_points = []
			for i in range(min_x, max_x):
				for j in range(min_y, max_y):
					# calculates a circle inside the box
					new_x = i*scale + offset_x
					new_y = j*scale + offset_y
					#print("point %d,%d" %(new_x, new_y))
					if ( (x-new_x)**2 + (y-new_y)**2 < radius**2 ):
						# the point is a cloud
						if ( raw_nfai.data[j,i] == -1000):
							cloud_points.append([new_x,new_y])

			# if there is clouds in the circle
			if len(cloud_points) != 0:
				# calculate the average cloud point position
				#for point in cloud_points:
				cloud_center = np.mean(cloud_points, axis=0)
				
				# ROUGH direction of clouds for this cluster
				cloud_vector = [cloud_center[0]-x, cloud_center[1]-y]
				angles.append(cloud_vector)

	# average cloud position [x,y] compared to the center of a sargassum
	avg_position = np.mean(angles, axis=0)
	
	# angle calculation (degrees) of the average position
	avg_angle = 0
	if avg_position[1] == 0:
		avg_angle = 90
	else:
		avg_angle = np.arctan(avg_position[0] / avg_position[1])*360/(2*np.pi)
		if avg_position[0] < 0:
			avg_angle += 180
		elif avg_position[1] < 0:
			avg_angle += 360
	
	print("[%.2f] AVERAGE ANGLE: %f with %d/%d samples used (%d OOB)" %((time.time() - t_start), avg_angle, len(angles), len(clusterArray), oob_counter ) )

	boolClusterArray = []
	# now each cluster gets its angle checked against the medium angle
	for cluster in clusterArray:
		[y,x] = np.mean(cluster, axis=0)
		
		# draws a line starting from the center of the cluster, following the average angle
		boolClusterArray.append(intersect_calculator(avg_angle, x, y, raw_nfai, scale, parameters))
			
	print("[%.2f] Artifacts processing finished, %d artifacts found out of %d clusters" % ((time.time() - t_start), boolClusterArray.count(True), len(clusterArray)) )
	return boolClusterArray

# calculates whether or not this sargassum meets a cloud in the direction of the "average angle"
def intersect_calculator(angle_nom, x0, y0, raw_nfai, scale, parameters):
	offset_x = parameters['min_lon']
	offset_y = parameters['min_lat']

	for angle in [angle_nom-22.5, angle_nom, angle_nom+22.5]:
		# slope
		if (x0 != 1):
			m = (np.tan(angle/360*2*np.pi) - y0) / (1 - x0)
		else:
			m = (2*np.tan(angle/360*2*np.pi) - y0) / (2 - x0)

		# number of steps without clouds/sargassum before calling it quits
		steps = 5

		# dx >= dx (slope < 1)
		if m <= 1:
			x = x0
		else:
			y = y0

		while steps > 0:
			# dx >= dx (slope < 1)
			if m <= 1:
				x += scale
				y = m*x + y0 - m*x0
			else:
				y += scale
				x = m*y + x0 - m*y0				

			# we hit a cloud
			new_x = int((x - offset_x)/scale)
			new_y = int((y - offset_y)/scale)
			try:
				if (raw_nfai.data[new_y, new_x] == -1000):
					return True
				# we hit water
				elif (raw_nfai.data[new_y, new_x] < parameters['level_raw']):
					steps -= 1
			# out of bounds
			except IndexError:
				#print("Out of bounds...")
				return False
		
	# no cloud was hit before steps ran out
	return False
	
# rounds to the closest number following the scale
def round_scale(x, prec=5, scale=.0025):
	return round(scale * round(float(x)/scale),prec)

# #################################
# Hull calculation for each cluster
# No hull on outliers
def calculate_hull(full_points):
	try:
		hull = ConvexHull(full_points, incremental=False)
		p = []

		# ############################
		# Saving the hull as a polygon
		for vertex in hull.vertices:
			p.append([full_points[vertex, 1], full_points[vertex, 0]])
		# shape returned
		return Polygon(p)
	# Hull calculation might fail
	except:
		return None

if __name__ == '__main__':
	
	# Check number of args
	if len(sys.argv) != 3 or sys.argv[1].find('-h') >= 0:
		# -h ou --help
		PROG = os.path.basename(sys.argv[0])
		print('Usage : ' + PROG + ' ' + PROG + '.yml'+ 'date for sample 2019-01-01')
		sys.exit(1)

	with open(sys.argv[1], 'r') as stream:
		try:
			parameters = yaml.load(stream)
		except yaml.YAMLError as exc:
			print(exc)

	# Actual execution
	try:
		# Creates a log file
		global my_logger 
		my_logger= logging.getLogger('sargassum-contouring')
		my_logger.setLevel(logging.INFO)
		
		# Log file error handling
		if sys.argv[1].find('-nolog')<0:
			logfile=parameters['log_output_directory']+'sargassumcontouring.log'
			logHandler = handlers.TimedRotatingFileHandler(logfile, when='midnight')        
		else:
			logHandler= logging.StreamHandler()
		   
		# Here we define our formatter
		formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')    
				 
		# Here we set our logHandler's formatter
		logHandler.setFormatter(formatter)      
		my_logger.addHandler(logHandler)

		
		#check the yaml integrity  
		if (len(parameters['dataset_name']) != len(parameters['dataset_sensor'])):
			my_logger.info('Number of dataset name and dataset sensor not equal')
			raise
		if not os.path.isdir(parameters['shapefile_directory']):
			my_logger.info('%s is not an available directory',parameters['shapefile_directory'])
			raise
		if not os.path.isdir(parameters['netcdf_output_directory']):
			my_logger.info('%s is not an available directory',parameters['netcdf_output_directory'])
			raise            
		if not os.path.isdir(parameters['mobidrift_output_directory']):
			my_logger.info('%s is not an available directory',parameters['mobidrift_output_directory'])
			raise 
		if not os.path.isdir(parameters['log_output_directory']):
			my_logger.info('%s is not an available directory',parameters['log_output_directory'])
			raise 

		#Date reader
		try:
			date= re.findall("([0-9]*)-([0-9]*)-([0-9]*)", sys.argv[2])
			parameters['dataset_year']=date[0][0]
			parameters['dataset_month']=date[0][1]
			parameters['dataset_day']=date[0][2]
		except:
			my_logger.error('Issue with the date format') 
		

		#Area nomenclature
		observationArea=''
		if float(parameters['min_lon'])<0:
			observationArea= observationArea + "W{:05d}".format(int(abs(float(parameters['min_lon']))*100))
		else:
			observationArea= observationArea + "E{:05d}".format(int(abs(float(parameters['min_lon']))*100))  
		if float(parameters['min_lat'])<0:
			observationArea= observationArea + "S{:04d}".format(int(abs(float(parameters['min_lat']))*100))
		else:
			observationArea= observationArea + "N{:04d}".format(int(abs(float(parameters['min_lat']))*100)) 
		if float(parameters['max_lon'])<0:
			observationArea= observationArea + "W{:05d}".format(int(abs(float(parameters['max_lon']))*100))
		else:
			observationArea= observationArea + "E{:05d}".format(int(abs(float(parameters['max_lon']))*100))    
		if float(parameters['max_lat'])<0:
			observationArea= observationArea + "S{:04d}".format(int(abs(float(parameters['max_lat']))*100))
		else:
			observationArea= observationArea + "N{:04d}".format(int(abs(float(parameters['max_lat']))*100))  
 
		output_directory = parameters['netcdf_output_directory']
		
		# loop on the datasets
		sensorName=""
		for dataset,sensorId in zip(parameters['dataset_name'],parameters['dataset_sensor']):
			#Netcdf management
			output_filenename=str(dataset) + '_' +  \
								  observationArea + '_' + \
								  str(parameters['dataset_year'])+str(parameters['dataset_month'])+str(parameters['dataset_day'])+'000000.nc'
		
			# NFAI dowloading if it's not already done
			if not os.path.isfile(output_directory+output_filenename):
				my_logger.info('Start Downloading from MOTU')
				downloading(parameters,dataset,output_directory,output_filenename)
				my_logger.info('End Downloading from MOTU')
			else:
				my_logger.info('file already existing: %s', output_directory+output_filenename)
				
			# Sargassum contouring process
			my_logger.info('Start contouring from file : %s', output_directory+output_filenename)
			
			# ##################
			# Shapefile creation
			if (parameters['mode'] == "contour"):
				contourPolygons=contouring(parameters,output_directory,output_filenename)
				# no contour calculation needed (already done)
				shapefile(parameters,observationArea,contourPolygons,sensorName)
			elif (parameters['mode'] == "remove_artifact"):
				clusterClasses=remove_artifact(parameters,output_directory,output_filenename)
				cluster_cluster(parameters, clusterClasses)
				polygonClasses = []
				# for each class
				for clusterClass in clusterClasses:
					polygonClass = []
					# for each cluster
					for cluster in clusterClass:
						# calculate the outline
						polygon = calculate_hull(cluster)
						if (polygon != None):
							polygonClass.append(polygon)
					# once filled, the class is added to the list of classes
					polygonClasses.append(polygonClass)

				# shapefile creation on the new contours
				shapefile(parameters,observationArea,polygonClasses,sensorName)
			else:
				print("Wrong mode selected, aborting contouring")
				my_logger.info("Wrong mode selected, aborting contouring")

		print("Time taken to run the program: %.2f s" % (time.time() - t_start) )

		my_logger.info('End contouring') 
		my_logger.info('SUCCEEDED') 
	except Exception as e:
		my_logger.error('the run is not completed')
		traceback.print_exc()
		print(e)
		sys.exit(1)