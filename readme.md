
# Ajouter la variable d'environement DATALOCAL_SARGAS dans le .bash_profile

cette variable est le chemein d'accés à l'espace de déploiement de sargassum contouring

```
export DATALOCAL_SARGAS=/mnt/f/Perso/INSA/LAAS_internship/sargas
```

# Creer un environement virtuel

- creation de l'environement :

```
virtualenv_PATH -p python3.6_PATH ${DATALOCAL_SARGAS}/sargassum-contouring/sargasse_virtualenv
.  ${DATALOCAL_SARGAS}/sargassum-contouring/sargasse_virtualenv/bin/activate
pip install -U
pip install numpy==1.15.4
pip install geopandas==0.4.0
pip install netCDF4==1.4.2
pip install scipy==1.1.0
pip install pyyaml==3.13
pip install scikit-image==0.14.1
pip install motuclient==1.8.1
pip install lxml==4.3.0
```

- Pour exécuter sargassun-contouring.py il faut activer l'environnement virtuel :

```
${DATALOCAL_SARGAS}/sargassum-contouring/sargasse_virtualenv/bin/activate
```

- Pour lancer de façon automatique sargassun-contouring.py il faut activer la crontab :

```
crontab ${DATALOCAL_SARGAS}/sargassum-contouring/tools/crontab.txt
```
