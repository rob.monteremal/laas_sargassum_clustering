# User guide

## CLS-SeeWater

0. Identification:
	- Login: 
	- Password: 

1. Satellite sargassum detection:
	- MODIS: satellite name
	- Cloud cover to show clouds (color greend)
	- Sargassum mats only to show sargassum(color red)
	- Each "category" is a geographical area
	- Resolution: 1px = 300x300m

2. Time window:
	- Top to bottom: year, month, day
	- Dot on top = data available for this date
	- Bot on the bottom = day selected (careful to check that data exists for this day and not just month)


## Python script

0. Setup:
	- Need to install pythonenv first with *pip3 install pipenv*
	- Follow readme.md
	- You may need to install the following :
		- Install skimage *pip3 install scikit-image*
		- Install netCDF4 *pip3 install netCDF4*
		- Install geopandas *pip3 install geopandas*
		- Install motuclient *pip3 install motuclient*
		- Install lmxl *pip3 install lxml*
		- Install dask *pip3 install dask*
		- Install dask dependencies *pip3 install dask[array] --upgrade*
	- Modifity *config/configuration_LAAS.yml*:
		- Change the paths for output data
		- **Change the dataset-name, as caribbean data isn't available anymore**

1. Running:
	- Run *python3 src/sargassum-contouring.py config/configuration_LAAS.yaml date* to generate the contouring shapefile
		- *date* needs to be replaced by a valid date (ex: 2019-07-01 in ISO format)
	- Run *python3 print_shapefiles.py* to print the shapefile with matplotlib
	- On CLS-SeeWater, you can import the shapefile (menu to the right) to display them over the map
		- Make sure to select the same date as the shapefile if you want to overlap the shapes with the sargassum
		
2. Important notes:
	- The actual countouring (skimage) is done with *measure.find_contours(g.data, level)*
		- *find_contours()* uses the marching square algorithm (*cf clustering.md*)
		- Contours are shifted by half a pixel (corner values vs center values)
	- *downloading()* downloads the netcdf files from Motu
	- *countouring()* reads the netcdf file and generates the shapes/polygons
	- *shapefile()* generates a shapefile from shapes/polygons
	- Run time is very long (a couple minutes for the "global" map)
	
	
3. Tweaking:
	- *level_min*: level used in the contouring task (minimum threshold to generate a contour)
		- 0.1 (with global map): 157 contours found, 64 final contours
		- 0.01 (with global map): 1300 contours found, 741 final contours
		- Low values create a lot of false positives, and massively increase the processing time
		
	- *level_max*: creates a polygon only if the max of the mask(?!) is above this
		- 0 (with global map): 157 contours found, 66 final contours
		- 0.03 (with global map): 157 contours found, 64 final contours
		- 0.5 (with global map): 157 contours found, 0 final contours

	- *contour_threlhold*: minimum number of sides needed for a polygon to be saved:
		- 12 (with global map): 64 final contours
		- 6 (with global map): 84 final contours
		- Minimum impact on performance, lowering the number helps contouring small (and simple) shapes
		
	- *maximum_number_polygon*: maximum number of shapes

	
## DBSCAN

0. Config:
	- eps = distance between each point to be considered in the same cluster (0.3 to 0.5 is a good range)
	- min_samples = number of points necessary to create a new cluster (default 12)
	- Check *DBSCAN_level_and_min.png* to see the diff between each value
	- filename: input file name
	- dir: input file path
	- scale: number of points per degree (*default is 0.00025*)
	- size: number of points used in Split mode
	
1. Plot mode:
	- **False**: a single plot is generated
	- **True**: a plot for every eps/min_samples combination is generated
	
2. Split mode:
	- **False**: normal DBSCAN
	- **True**: the area is split in a grid of *size* x *size* number of points and DBSCAN is run on each area
	
3. Hull mode:
	- **False**: no convex hull calculation
	- **True**: the convex hull of each cluster is calculated and written to a shapefile
	
4. Show plot:
	- **False**: no display
	- **True**: display plot of clusters and/or hulls
	
